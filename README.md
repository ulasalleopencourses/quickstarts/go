# QuickStarts Go

## Objetivo

El objetivo de crear este web service en golang para un QuickStart es que las personas que busquen aprender un lenguaje poderoso como lo es golang puedan empezar con algo que es muy usado como son los web services. 

## Descripción

Web service en golang usando como base de datos sqlite. Se usa una tabla de notas para manejar post, get, put y delete desde el web services de go.

## Carpetas

    * Videos: 
        * Unidad 1: Instalación de golang y primeros pasos en el lenguaje.
        * Unidad 2: Creación de web services sin base de datos.
        * Unidad 3: Instalación de mingw, sqlite, integración en web services y prueba en aplicativo de telefono hecho en flutter.
    * Src:
        * Código fuente del web service.

## Creador
    
    * Nombre: Walker Manrique Chalco
    * Correo: wmanriquec@gmail.com